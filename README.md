# Twitter Grammy XDoc Corpus

A corpus of Twitter posts about the Grammy Award ceromony.

Splits of the annotated data (by entity):

1. 1374 mentions, 55 entities
2. 910 mentions, 55 entities
3. 717 mentions, 55 entities
4. 968 mentions, 55 entities
5. 608 mentions, 54 entities

The splits were obtained by first sorting the entities by # of
mentions, then doing systematic sampling of the entities on the sorted
list.

The first split is reserved for train/dev purposes. The remaining
splits are reserved for testing.

# References

Mark Dredze, Nicholas Andrews, Jay DeYoung. Twitter at the Grammys: A
Social Media Corpus for Entity Linking and Disambiguation. EMNLP
Workshop on Natural Language Processing for Social Media, 2016
